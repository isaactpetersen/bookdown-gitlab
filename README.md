[![Build Status](https://travis-ci.com/rstudio/bookdown-demo.svg?branch=master)](https://travis-ci.com/rstudio/bookdown-demo)

This is a minimal example of a book based on R Markdown and **bookdown** that is deployed to GitLab pages: https://isaactpetersen.gitlab.io/bookdown-gitlab/

(As adapted from instructions here: https://rlesur.gitlab.io/bookdown-gitlab-pages/ and https://gitlab.com/RLesur/bookdown-gitlab-pages/)
